(setq delete-old-versions -1)
(setq version-control t)	
(setq vc-make-backup-files t)
(setq backup-directory-alist `(("." . "~/.emacs.d/backups")))
(setq vc-follow-symlinks t)
(setq auto-save-file-name-transforms '((".*" "~/.emacs.d/auto-save-list/" t)))
(setq inhibit-startup-screen t)
(setq ring-bell-function 'ignore)
(setq coding-system-for-read 'utf-8)
(setq coding-system-for-write 'utf-8)
(setq sentence-end-double-space nil)
(setq default-fill-column 80)
(setq initial-scratch-message "")

(require 'package)
(setq package-enable-at-startup nil)
(setq package-archives '(("org"       . "http://orgmode.org/elpa/")
                         ("gnu"       . "http://elpa.gnu.org/packages/")
                         ("melpa"     . "https://melpa.org/packages/")))
(package-initialize)

;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; package management
(require 'use-package)

;; keybind management
(use-package general
  :ensure t)

;; keybind discovery
(use-package which-key
  :ensure t
  :config
  (which-key-mode)
  (setq which-key-idle-delay 0.6)
  (setq which-key-separator ":")
  (setq which-key-prefix-prefix ""))

;; autocomplete
(use-package ivy :demand
  :config
  (setq ivy-use-virtual-buffers t
	ivy-count-format "%d/%d "))

;; project management
(use-package projectile
  :ensure t
  :config
  (setq projectile-completion-system 'ivy)
  (projectile-mode +1))

;; project integration with counsel
(use-package counsel-projectile
  :ensure t
  :config
  (counsel-projectile-mode))

;; git
(use-package magit
  :ensure t)

;; what it says on the tin
(use-package clojure-mode
  :ensure t)

;; clojure repl integration
(use-package cider
  :ensure t :defer t)

;; vim mode
(use-package evil :ensure t
  :config (evil-mode 1))

;; vim mode surround editor (brackets, quotes etc)
(use-package evil-surround
  :ensure t)

;; status bar
(use-package smart-mode-line
  :ensure t
  :config
  (setq sml/no-confirm-load-theme t)
  (setq sml/theme 'dark)
  (sml/setup))

;; pretty colours
(use-package monokai-theme
  :ensure t
  :config (load-theme 'monokai t))

;; re-indent as you type
(use-package aggressive-indent
  :ensure t :defer t
  :hook (clojure-mode emacs-lisp-mode))

;; org mode
(use-package org :ensure t :defer t)

;; smarter parentheses in lisp languages
(use-package smartparens
  :ensure t :defer t
  :config (require 'smartparens-config)
  :hook (emacs-lisp-mode clojure-mode))

;; hide some app chrome
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

;; save state between session
(desktop-save-mode 1)

;; https://emacs.stackexchange.com/questions/38319/unable-to-bind-spc-as-a-prefix-in-general-evil-mode
;; see note about this blocking being able to enter a space in evil-emacs-state
(general-define-key
 :states '(normal visual emacs)
 :prefix "SPC"

 "SPC" '(counsel-M-x :which-key "M-x")
 "ESC" '(keyboard-escape-quit :which-key "ESC")
 "TAB" '(mode-line-other-buffer :which-key "Last Buffer")

 "b" '(:ignore t :which-key "Buffer")
 "bb" '(counsel-ibuffer :which-key "Switch")
 "bk" '(kill-buffer :which-key "Kill")

 "e" '(:ignore t :which-key "Eval")
 "eb" '(eval-buffer :which-key "Buffer")
 
 "f" '(:ignore t :which-key "Files")
 "ff" '(counsel-find-file :which-key "Find")
 "fr" '(counsel-recentf :which-key "Recent")
 "fs" '(save-buffer :which-key "Save")

 "p" '(:ignore t :which-key "Project")
 "pf" '(counsel-projectile-find-file :which-key "Find File")
 "pp" '(counsel-projectile-switch-project :which-key "Switch Project")

 "s" '(:ignore t :which-key "Search")
 "sp" '(counsel-projectile-rg :which-key "Search Project")
 ) 

(setq evil-normal-state-tag
      (propertize " N " 'face '((:background "DarkGoldenrod2" :foreground "black")))

      evil-emacs-state-tag
      (propertize " E " 'face '((:background "SkyBlue2"       :foreground "black")))

      evil-insert-state-tag
      (propertize " I " 'face '((:background "chartreuse3"    :foreground "black")))

      evil-replace-state-tag
      (propertize " R " 'face '((:background "chocolate"      :foreground "black")))

      evil-motion-state-tag
      (propertize " M " 'face '((:background "plum3"          :foreground "black")))

      evil-visual-state-tag
      (propertize " V " 'face '((:background "gray"           :foreground "black")))

      evil-operator-state-tag
      (propertize " O " 'face '((:background "sandy brown"    :foreground "black"))))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" default)))
 '(package-selected-packages
   (quote
    (magit smartparens aggressive-indent aggressive-indent-mode monokai-theme counsel-projectile counsel smart-mode-line use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
